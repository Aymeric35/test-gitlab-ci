package com.example.restservice;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.*;

@RestController
public class GreetingController {
	private List<String> users = new ArrayList<>();
	private static final String template = "Hello, %s!";
	private final AtomicLong counter = new AtomicLong();

	@GetMapping("/greeting")
	public List<String> greeting() {
		return users;
	}

	@PostMapping("/greeting")
	public Greeting update(@RequestParam(value = "name") String name) {
		users.add(name);
		return new Greeting(counter.incrementAndGet(), "User added");
	}

	@PutMapping("/greeting")
	public Greeting put(@RequestParam(value = "name") String name) {
		users.set(users.indexOf(name), "Test");
		return new Greeting(counter.incrementAndGet(), "User modified");
	}

	@DeleteMapping("/greeting")
	public Greeting delete(@RequestParam(value = "name") String name) {
		users.remove(name);
		return new Greeting(counter.incrementAndGet(), "User deleted");
	}
}
